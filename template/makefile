SHELL=/bin/bash
include .env
BOX_FILE := $(BOX_NAME)_$(VERSION).box
build_home := ./build
build_target := $(build_home)/target

.PHONY: help clean start stop build package upload 

help:
	echo "make [up|down|build|install|publish]unpublish|prepublish|release"

start:
	vagrant up

stop:
	vagrant halt

destroy:
	vagrant destroy -f

build: 
	mkdir -p $(build_target)/
	vagrant up
	vagrant package --output $(build_target)/$(BOX_FILE)

install:
	vagrant box add -f -a amd64 --provider virtualbox --name $(ORGANIZATION)/$(BOX_NAME) $(build_target)/$(BOX_FILE)

uninstall: 
	vagrant box remove -f --all $(ORGANIZATION)/$(BOX_NAME)

clean:
	make destroy 
	rm -rf $(build_target)/*

onestep-install:
	make clean build install 

cloud_login:
	vagrant cloud auth login

cloud_create_box:
	vagrant cloud box create \
		-d "$(ORGANIZATION)/$(BOX_NAME)" \
		-s "simple description" \
		--no-private \
		$(ORGANIZATION)/$(BOX_NAME)

cloud_create_version:
	vagrant cloud version create \
		-d "$(ORGANIZATION)/$(BOX_NAME) $(VERSION)" \
		$(ORGANIZATION)/$(BOX_NAME) $(VERSION)

cloud_create_provider:
	vagrant cloud provider create \
		--architecture amd64 \
		--default-architecture \
		$(ORGANIZATION)/$(BOX_NAME) \
		virtualbox $(VERSION) 

cloud_upload_box:
	vagrant cloud provider upload \
		$(ORGANIZATION)/$(BOX_NAME) virtualbox $(VERSION) \
		$(build_target)/$(BOX_FILE) \

cloud_delete_provider:
	vagrant cloud provider delete \
		-f \
		$(ORGANIZATION)/$(BOX_NAME) virtualbox $(VERSION) \
		amd64

cloud_delete_box:
	vagrant cloud box delete -f $(ORGANIZATION)/$(BOX_NAME)

.ONESHELL:
cloud_publish:
	vagrant cloud publish \
		-f \
        --no-private \
		-s "$(BOX_NAME) $(VERSION)" \
		-d "$(BOX_NAME) $(VERSION)" \
		--description "$(BOX_NAME)" \
		--version-description "$(BOX_NAME) $(VERSION)" \
		--box-version $(VERSION) \
		$(ORGANIZATION)/$(BOX_NAME) $(VERSION) virtualbox \
		$(build_target)/$(BOX_FILE)

cloud_release: 
	vagrant cloud version release $(ORGANIZATION)/$(BOX_NAME) $(VERSION)

cloud_revoke: 
	vagrant cloud version revoke $(ORGANIZATION)/$(BOX_NAME) $(VERSION)
